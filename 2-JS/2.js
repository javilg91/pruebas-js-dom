'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

function resultados() {
  for (let puntuacion of puntuaciones) {
    Object.assign(puntuacion, { total: 0 });

    document.body.innerHTML = JSON.stringify(puntuaciones);
    puntuacion.puntos.forEach((t) => (puntuacion.total += t));
  }

  puntuaciones.sort((a, b) => {
    if (a.total < b.total) {
      return -1;
    }
    if (a.total > b.total) {
      return 1;
    }
    return 0;
  });

  console.log(
    `El equipo ${puntuaciones[0].equipo} fue el equipo con menos puntos: ${
      puntuaciones[0].total
    }, mientras que el equipo ${
      puntuaciones[puntuaciones.length - 1].equipo
    } fue el que más puntos obtuvo: ${
      puntuaciones[puntuaciones.length - 1].total
    }`
  );
}

resultados();
