'use strict';
const reloj = document.createElement('div');
reloj.classList.add('clock');
const body = document.querySelector('body');
body.appendChild(reloj);
function startTime() {
  var today = new Date();
  var hr = today.getHours();
  var min = today.getMinutes();
  var sec = today.getSeconds();
  //Add a zero in front of numbers<10
  sec = checkTime(sec);
  min = checkTime(min);
  reloj.innerHTML = hr + ' : ' + min + ' : ' + sec;
  var time = setTimeout(function () {
    startTime();
  }, 1000);
}
function checkTime(i) {
  if (i < 10) {
    i = '0' + i;
  }
  return i;
}
startTime();
