'use strict';

function contraseña() {
  const contraseña = Math.floor(Math.random() * 101);
  let login;
  let cont = 0;
  while (cont < 5) {
    login = parseInt(prompt('Numero?'));
    if (login === contraseña) {
      alert('Has ganado! El número era: ' + contraseña);
      break;
    } else {
      alert(login > contraseña ? 'Es MENOR' : 'Es MAYOR');
      cont++;
      if (cont === 5) {
        alert('Has agotado el número de intentos');
      }
    }
  }
}

contraseña();
